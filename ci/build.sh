set -v

npm install
npm run build

mkdir public -p
mv ./dist/* ./public/
mv ./client/vendors ./public/
mv ./client/assets ./public
cp ./client/sitemap.xml ./public
cp ./client/robots.txt ./public
mv ./signature ./public
# mv ./client/client/404.html ./public
# mv ./client/client/500.html ./public
mv ./bower.json ./public/vendors
rm .bowerrc
bower install --config.cwd=./public/vendors --allow-root
