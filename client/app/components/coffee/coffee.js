import angular from 'angular';
import coffeeComponent from './coffee.component';

let coffeeModule = angular.module('coffee', [
])
  .component('coffee', coffeeComponent)
  .name;

export default coffeeModule;
