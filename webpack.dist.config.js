var webpack = require('webpack');
var path = require('path');
var config = require('./webpack.config');

config.output = {
  filename: '[name].[contenthash].bundle.js',
  publicPath: '',
  path: path.resolve(__dirname, 'dist')
};

config.optimization = {
  splitChunks: {
    chunks: 'all'
  },
  runtimeChunk: 'single',
  moduleIds: 'hashed'
};

config.mode = 'production';

module.exports = config;
